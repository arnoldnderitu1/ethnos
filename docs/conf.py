# -- Project information -----------------------------------------------------

project = 'Ethnos'
copyright = '2021, Arnold Nderitu'
author = 'Arnold Nderitu'


# -- General configuration ---------------------------------------------------

extensions = [
    'sphinx.ext.autosectionlabel',
]
autosectionlabel_prefix_document = True
templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

html_static_path = ['_static']
html_theme = 'alabaster'
html_theme_options = {
    'gitlab_user': 'arnoldnderitu1',
    'github_repo': 'ethnos',
    'github_banner': True,
    'github_button': False,
    'show_relbar_bottom': True,
    'show_powered_by': False,
    'extra_nav_links': {
        'Demo': 'https://ethnos.chat',
        'Source code': 'https://gitlab.com/arnoldnderitu1/ethnos',
        'Issue tracker': 'https://gitlab.com/arnoldnderitu1/ethnos/-/issues',        
    },
}
