Ethnos
=====

Ethnos is a video chat app that uses `WebRTC`_ for direct peer-to-peer
communication. Users can create public or private rooms, optionally protected
by a password. In addition to streaming audio and video from a webcam and
microphone, Ethnos also provides screen sharing and text chat.

Features
--------

-  Webcam streaming
-  Desktop sharing
-  Text chat
-  Room management (public/private, password/no password, guest limits)

Demo
----

You can find a live demo at https://ethnos.arnoldnderitu.com.

Running
-------
