# builder
FROM python:3.9-alpine as builder
RUN apk add make
ADD . /app
WORKDIR /app
RUN make package


# prod
FROM python:3.9-alpine as prod
ENV QUART_APP ethnos
ENV QUART_ENV production
COPY --from=builder /app/dist/ethnos*.tar.gz /root
RUN pip install /root/ethnos*.tar.gz
CMD /usr/local/bin/hypercorn ethnos --log-file - -b 0.0.0.0:5000
